export const request_params = function (headers) {
    if(!headers)
        headers={};
    return {
        credentials: "same-origin",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            ...headers,
        },
        mode: 'cors',
        cache: 'default'
    }
};
export const host = /*process.NODE_ENV="production"?'':*/'http://localhost:3000';
export const statuses ={
        denied: "Отклонен",
        approved: "Подтвержден",
        pending: "Проверка преподавателем",
        moderation: "Проверка администратором"
};



