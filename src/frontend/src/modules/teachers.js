import {host, request_params} from "../../config/index";
import {checkErr, handleErr} from "./index";
export const TEACHERS = "teachers";
export const GROUPS = "groups";
const initialState = {
   teachers:null
};
export default (state = initialState, action) => {
    switch (action.type) {
        case TEACHERS:
            return {
                ...state,
                teachers: action.teachers
            };
        case GROUPS:
            return {
                ...state,
                groups: action.groups
            };
        default:
            return state
    }
}
export const loadTeachers = () => {
    return dispatch => {
        return fetch(host + "/teachers", {
            ...request_params({"x-access-token": localStorage.getItem("jwt_token_fspo")}),
            method: 'GET',
        }).then(res => res.json()).then(checkErr)
            .then((res) => {
                dispatch({
                    teachers: res.teachers,
                    type: TEACHERS
                })
            }).catch(e=>dispatch(handleErr(e)));
    }
};

export const loadGroups = () => {
    return dispatch => {
        return fetch(host + "/groups", {
            ...request_params(),
            method: 'GET',
        }).then(res => res.json()).then(checkErr)
            .then((res) => {
                dispatch({
                    groups:res.groups,
                    type: GROUPS
                })
            }).catch(e=>dispatch(handleErr(e)));
    }
};
