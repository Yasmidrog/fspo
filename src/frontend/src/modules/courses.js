import {request_params, host} from '../../config/index'
import {checkErr, handleErr} from './index'
import {statuses} from "../../config/index";

export const COURSES = "courses";
export const MY_APPLICATIONS = "my_app";
export const EDITING_COURSE = "current_app";
export const ALL_APPS = "apps_for_me";
export const SET_STATE = "set state";
export const SUBMIT_TEST = "submit_test";
export const SUBMIT_CERT_MESG = "submit cert mesg";
const initialState = {
    courses: null,
    myApplications: null,
    allApps: null,
    editingCourse: null,
    submitMessage: ""
};

export default (state = initialState, action) => {
    switch (action.type) {

        case COURSES:
            let o = {
                ...state,
                courses: action.courses
            };
            if (!!action.courses && !!state.editingCourse)
                for (const [i, a] of action.courses.entries())
                    if (a.course_id === state.editingCourse.course_id)
                        o.editingCourse = action.courses[i];

            return o;
        case MY_APPLICATIONS:
            return {
                ...state,
                myApplications: action.apps
            };
        case SUBMIT_CERT_MESG:
            return {
                ...state,
                submitMessage: action.mesg
            };

        case ALL_APPS:
            return {
                ...state,
                allApps: action.apps
            };
        case SET_STATE:
            let s = {...state, allApps: [...state.allApps]};
            for (const [i, a] of s.allApps.entries())
                if (a.application_id === action.application.application_id)
                    s.allApps[i] = {...action.application};
            return s;

        case EDITING_COURSE:
            return {
                ...state,
                editingCourse: action.course
            };


        default:
            return state;
    }
};


export const loadCourses = (group, by_teacher, admins) => {
    let q = "";
    if (group)
        q = `group=${group}`;
    else if (by_teacher)
        q = "by_teacher=true";
    else if (admins)
        q = "admins=true";
    return dispatch => {
        fetchCourses(dispatch, q)
            .catch(e => dispatch(handleErr(e)));
    }
};
export const delCourse = (id) => {
    return dispatch => {
        return fetch(host + `/admin/courses/${id}/delete`, {
            ...request_params({'x-access-token': localStorage.getItem('jwt_token_fspo')}),
            method: 'GET',
        }).then(res => res.json()).then(checkErr).then(() => fetchCourses(dispatch)).catch(e => dispatch(handleErr(e)));
    }
};

function fetchCourses(dispatch, query) {
    return fetch(host + "/courses" + (query ? ("?" + query) : "")
        , {
            ...request_params({"x-access-token": localStorage.getItem("jwt_token_fspo")}),
            method: 'GET',
        }).then(res => res.json()).then(checkErr).then((res) => {
        dispatch({
            courses: res.courses,
            type: COURSES
        })
    })
}

function fetchApplications(dispatch, query, type) {
    return fetch(host + '/applications' + (query ? ('?' + query) : ''), {
        ...request_params({'x-access-token': localStorage.getItem('jwt_token_fspo')}),
        method: 'GET',
    }).then(res => res.json()).then(checkErr).then((res) => {
        dispatch({
            type: type || MY_APPLICATIONS,
            apps: res.applications
        })
    })
}

export const submitCourse = (course) => {

    return dispatch => {
        return fetch(host + "/admin/courses/new", {
            ...request_params({'x-access-token': localStorage.getItem('jwt_token_fspo')}),
            method: 'POST',
            body: JSON.stringify(course)
        }).then(res => res.json()).then(checkErr)
            .then(() => {

                dispatch({
                    type: "validation",
                    error: undefined,
                });
                alert("Курс добавлен!");

            })
            .then(() => fetchCourses(dispatch))
            .catch(e => dispatch(handleErr(e)));
    }
};

export const submitApplication = (teacher_id, link, file) => {

    return dispatch => {
        const formData = new FormData();
        if (teacher_id)
            formData.append("teacher_id", teacher_id);
        if (link) formData.append("link", link);
        if (file) formData.append("file", file);

        return fetch(host + "/applications/new", {
                credentials: "same-origin",
                mode: 'cors',
                headers: {
                    "x-access-token": localStorage.getItem("jwt_token_fspo")
                },
                method: 'POST',
                body: formData,
            }
        ).then(res => res.json())
            .then(checkErr).then(() => {
                dispatch({
                    type: SUBMIT_CERT_MESG,
                    mesg: "Успешно отправлено"
                })
            })
            .catch(e => {
                dispatch({
                    type: SUBMIT_CERT_MESG,
                    mesg: "Произошла ошибка"
                });
                dispatch(handleErr(e))
            });
    }
};

export const getMyApplications = () => {
    return dispatch => {
        fetchApplications(dispatch, `type=my`)
            .catch(e => dispatch(handleErr(e)));
    }
};
export const getApplicationsForMe = () => {
    return dispatch => {
        fetchApplications(dispatch, 'type=forme', ALL_APPS)
            .catch(e => dispatch(handleErr(e)));
    }
};

export const setEnrolled = (course_id, enroll) => {
    return dispatch => {
        return fetch(host + '/enroll', {
            ...request_params({'x-access-token': localStorage.getItem('jwt_token_fspo')}),
            method: 'POST',
            body: JSON.stringify({
                course_id,
                enroll
            })
        }).then(res => res.json())
            .then(checkErr)
            .then(() => fetchCourses(dispatch))
            .then(checkErr)
            .catch(e => dispatch(handleErr(e)));
    }
};



export const submitTest = (questions, course) =>{
    let body = {
        questions,
        course_id:course.course_id,
    };

    if(!!course.test)
        body.test_id=course.test._id;

    return dispatch => {
        return fetch(host + '/tests/new', {
            ...request_params({'x-access-token': localStorage.getItem('jwt_token_fspo')}),
            method: 'POST',
            body: JSON.stringify(body)
        }).then(res => res.json())
            .then(checkErr)
            .then(() => {
                dispatch({
                    type: SUBMIT_TEST,
                })
            }).catch(e => {
                dispatch(handleErr(e))
            });
    }
};

export const submitAnswers = (answers, app) =>{
    let body = {
        answers,
        test_id:app.test._id,
    };

    if(!!app.test_result)
        body.res_id=app.test_result._id;

    return dispatch => {
        return fetch(host + '/tests/ans', {
            ...request_params({'x-access-token': localStorage.getItem('jwt_token_fspo')}),
            method: 'POST',
            body: JSON.stringify(body)
        }).then(res => res.json())
            .then(checkErr)
            .then(() => {
                dispatch({
                    type: SUBMIT_TEST,
                })
            }).catch(e => {
                dispatch(handleErr(e))
            });
    }
};



export const setStatus = (status, application, course, comment) => {
    let body = {
        status,
        application_id: application.application_id
    };
    if (course)
        body.course_id = course.course_id;
    if (comment)
        body.comment = comment;

    return dispatch => {
        return fetch(host + '/applications/setStatus', {
            ...request_params({'x-access-token': localStorage.getItem('jwt_token_fspo')}),
            method: 'POST',
            body: JSON.stringify(body)
        }).then(res => res.json())
            .then(checkErr)
            .then(() => {
                application.status_code = status;
                application.status = statuses[status];
                application.course_link = course.link;
                application.course_name = course.name;
                application.course_id = course.course_id;

                dispatch({
                    type: SET_STATE,
                    application: {...application}
                })
            }).catch(e => {
                dispatch(handleErr(e))
            });
    }
};

export const setEditingCourse = (course) => {
    return {
        type: EDITING_COURSE,
        course
    }
};
export const clearMessage = () => {
    return {
        type: SUBMIT_CERT_MESG,
        mesg:""
    }
};