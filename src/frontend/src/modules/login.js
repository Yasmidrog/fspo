import {request_params, host} from '../../config/index'
import {AUTH_ERROR, checkErr, handleErr} from "./index";
import {SUBMIT_CERT_MESG} from "./courses";

export const LOGIN="login";
export const LOGOUT="logout";

const initialState = {
    isLoggedIn:false,
    user:null,
};

export default (state = initialState, action) => {
  switch (action.type) {
      case LOGIN:
          return {
              ...state,
              isLoggedIn:true,
              user: action.user
          };
      case LOGOUT:
          localStorage.removeItem("jwt_token_fspo");
          return {
              ...state,
              isLoggedIn:false,
              user: null
          };
    default:
      return state;
  }
};


export const logout=()=>{
    return dispatch=>{
        dispatch({
            type:AUTH_ERROR,
            error:null,
        });
        dispatch({
            type: LOGOUT
        })
    }
};
export const login=(username, password)=>{
    return dispatch=> {
        fetch(host+"/login", {
            ...request_params(),
            method: 'POST',
            body: JSON.stringify({username, password}),
        }).then(res=>res.json()).
          then(checkErr).
          then((res) => {
            localStorage.setItem("jwt_token_fspo", res.jwt);
            dispatch({
                type:AUTH_ERROR,
                error:null,
            });
            dispatch({
                user: res.user,
                type: LOGIN
            });
            dispatch({
                type:SUBMIT_CERT_MESG,
                mesg:""
            })
        }).catch(e=>dispatch(handleErr(e)));
    }
};




