import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import login, {LOGIN} from "./login";
import {LOGOUT} from "./login";
import courses, {EDITING_COURSE} from './courses';
import teachers from './teachers'
export const ERROR="error";
export const AUTH_ERROR="autherr";
export const VALID_ERROR="validation";
export const SET_TAB="set_tab";


export function checkErr(json){
    if(json.status!==200) {
        let e = new Error(json.message);
        e.status = json.status;
      return Promise.reject(e);
    }

    return json
}
export function handleErr(e){
    if(e.message==="Invalid token")
        return{
         type:LOGOUT
        };
    else if(e.message.includes("Validation")){
        return{
            type:VALID_ERROR,
            error:e
        }
    }
    else
        return{
            type: e.status===401||e.status===403?AUTH_ERROR:ERROR,
            error:e
        };
}
const initialState = {
    error:null,
    authError: null,
    validationError:null
};
const errorReducer= (state = initialState, action) => {
    switch (action.type) {

        case ERROR:
            return {
                ...state,
                error: action.error
            };
        case VALID_ERROR:
            return{
                ...state,
                validationError: action.error
            };
        case AUTH_ERROR:
            return {
                ...state,
                authError: action.error
            };
        default: return state
    }
};

export const setTab = (tabNum) => {
    return {
        type: SET_TAB,
        tabNum
    }
};

const tabs=(state = {tab:1}, action) => {
    switch (action.type) {
        case SET_TAB:
            return {
                ...state,
                tab:action.tabNum
            };
        default:
            return state;
    }
};

export default combineReducers({
    router: routerReducer,
    courses,
    login,
    errors:errorReducer,
    teachers,
    tabs
});
