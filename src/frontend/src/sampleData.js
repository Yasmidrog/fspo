const apps=[
    {
        name:"Матан 1",
        status:"moderation",
        author:{
            name:"Саенко А.И",
            group: "Y2335"
        }
    },
    {
        name:"Физика для маленьких и отчаявшихся",
        status:"pending",
        author:{
            name:"Понаэтов С.В",
            group: "Y2335"
        }
    },
    {
        name:"Английский в картинках",
        status:"approved",
        author:{
            name:"Жан-Поль Препод"
        }
    },
    {
        name:"Психология маньяков",
        status:"denied",
        author:{
            name:"Жан-Поль Препод"
        }
    },
    {
        name:"Матан 1",
        status:"moderation",
        author:{
            name:"Попов Данила",
            group: "Y2335"
        }
    },
    {
        name:"Матан 1",
        status:"pending",
        author:{
            name:"Попов Данила",
            group: "Y2335"
        }
    },
    {
        name:"Английский в картинках",
        status:"denied",
        author:{
            name:"Вася Пупкин",
            group: "Y2331"
        }
    },
    {
        name:"Английский в картинках",
        status:"approved",
        author:{
            name:"Попов Данила",
            group: "Y2335"
        }
    }
];

const courses=[
    {
        name:"Курс  1",
        teacher:"Препод_имя Препод_Фамил",
        link:"http://coursera.org",
        status:true
    },
    {
        name:"Math thinking",
        teacher:"Романов",
        link:"http://coursera.org",
        status: false

    },
    {
        name:"Матан",
        teacher:"Волчек Д.Г.",
        link:"http://coursera.com",
        status:true
    }
];

export {apps,courses}