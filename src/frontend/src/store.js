import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import rootReducer from './modules/index';
import persistState from 'redux-localstorage'
export const history = createHistory();

const initialState = {};
const enhancers = [];
const middleware = [thunk, routerMiddleware(history)];

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.devToolsExtension;

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }
}

enhancers.push(persistState(undefined, {
    slicer: function () {
        return state => {
            delete state.login.err;
            delete state.error;

            if(!state.login.user||!state.login.isLoggedIn) {
               state.isLoggedIn=state.user=undefined;
            }
            return {...state}
        }
    }
}));

const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);

export default createStore(rootReducer, initialState, composedEnhancers);
