
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import store, { history } from './store';
import Home from './components/home';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import registerServiceWorker from './registerServiceWorker';

const target = document.querySelector('#root');
document.title="Сертификация";




render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <div>
                <Home />
            </div>
        </ConnectedRouter>

    </Provider>,
    target
);
(function() {
    var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
    link.type = 'image/x-icon';
    link.rel = 'shortcut icon';
    link.href = 'https://ifspo.ifmo.ru/img/favicon.ico';
    document.getElementsByTagName('head')[0].appendChild(link);
})();
registerServiceWorker();




