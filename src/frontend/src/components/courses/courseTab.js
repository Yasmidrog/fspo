import React from 'react';
import {Input, Table} from 'reactstrap'
import ReactTooltip from 'react-tooltip'
import CreateTestForm from "./createTestForm";

export default class CourseTab extends React.Component {
    constructor(props) {

        super(props);
        this.state = {
            teacher_filter: "",
            name_filter: "",
            status_filer: "",
            test_open: false
        };
        this.teachersTH = this.teachersTH.bind(this);
    }

    teachersTH(course) {
        if (course.teachers && course.teachers.length > 0) {
            let l = course.teachers.length;
            let f = course.teachers[0];
            let n = f.lastname + " " + f.firstname.charAt(0) + ". " + f.middlename.charAt(0) + ".";
            if (l > 1)
                n += " и еще " + (l - 1) + "...";
            return n;
        } else
            return "нет"
    }

    render() {

        if (!this.props.courses)
            return <div/>
        let courses = !this.state.teacher_filter ? this.props.courses :
            this.props.courses.filter((c) => {
                if (!c.teachers)
                    return false;
                for (let t of c.teachers)
                    if (t.teacher_id == this.state.teacher_filter)
                        return true;

                return false;
            });

        if (!!this.state.status_filter) {
            courses = courses.filter((c) => {
                    return c.status === this.state.status_filter
                }
            )
        }
        if (this.state.name_filter) {
            courses = courses.filter((c) => c.name.toLowerCase().includes(this.state.name_filter.toLowerCase()))
        }
        return (
            <div className={"table-wrap"}>
                <ReactTooltip/>
                <Table>
                    <thead>
                    <tr>
                        <th>Название:
                            <Input value={this.state.name_filter}
                                   onChange={(e) => this.setState({
                                       ...this.state,
                                       name_filter: e.target.value
                                   })}
                                   type="text"
                                   name="name_filter"/>
                        </th>

                        <th>
                            Преподаватель:
                            <Input className={"course-teacher-select"} value={this.state.teacher_filter}
                                   onChange={(e) => this.setState({
                                       ...this.state,
                                       teacher_filter: e.target.value
                                   })}
                                   type="select"
                                   name="teacher_filter">
                                <option value={""}>Все</option>
                                {!!this.props.teachers &&
                                this.props.teachers.map((t, i) => (
                                    <option value={t.user_id}
                                            key={t.user_id + "c"}>
                                        {t.lastname + " " + t.firstname + " " + t.middlename}
                                    </option>
                                ))
                                }
                            </Input>
                        </th>
                        <th>
                            <label className={"checkmark-label"}>Статус: <input
                                onChange={(e) => this.setState({
                                    ...this.state,
                                    status_filter: e.target.checked ? "enrolled" : ""
                                })}
                                type="checkbox"
                                name="status_filter"
                            />
                                <div className={'checkmark'}/>
                            </label>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {courses && courses.map((c) => (
                        <tr key={c.link + " " + c.name}>
                            <th scope="row">
                                <a target={"_blank"} href={c.link}>{c.name}</a>
                                <span className={"course-admin-btns"}>
                                    <span data-tip={c.desc} className={"text-muted"}>
                                        Комментарий
                                    </span>
                                    {this.props.isAdmin &&
                                    <div onClick={() => {
                                        this.props.delCourse(c.course_id);
                                    }} className={"delete-circle"} style={{display: "inline", cursor: "pointer"}}>
                                        <span className={"glyphicon glyphicon-remove"}></span>
                                    </div>
                                    }
                                    {this.props.isAdmin &&
                                    <div onClick={() => {
                                        this.props.setEditCourse(c)
                                    }} className={"delete-circle"} style={{display: "inline", cursor: "pointer"}}>
                                        <span className={"glyphicon glyphicon-pencil"}/>
                                    </div>
                                    }
                                    {this.props.isTeacher &&
                                    <div onClick={() => {
                                        this.setState({...this.state, test_open: c})
                                    }} style={{display: "inline", cursor: "pointer"}}>
                                        {c.test ? "[Редактировать тест]" : "[Добавить тест]"}
                                    </div>
                                    }
                               </span>

                            </th>
                            <td>{c.teachers &&
                            <span style={{
                                textDecoration: "underline"
                            }} data-tip={
                                ((tc) =>
                                        tc.reduce((acc, t) =>
                                            acc + t.lastname + " " + t.firstname.charAt(0) + ". " + t.middlename.charAt(0) + "., ", "")
                                )(c.teachers)
                            }>{
                                this.teachersTH(c)
                            }</span>
                            }
                            </td>
                            <td>
                                <label className={"checkmark-label"}>
                                    Прохожу:
                                    <input type={"checkbox"}
                                           defaultChecked={c.status === "enrolled"}
                                           onChange={e => {
                                               this.props.setEnrolled(c.course_id, e.target.checked)
                                           }}/>
                                    <div className={'checkmark'}></div>
                                </label>
                            </td>
                        </tr>
                    ))
                    }
                    </tbody>
                </Table>
                <CreateTestForm
                    close={() => {
                        this.setState({...this.state, test_open: false})
                    }}
                    course={this.state.test_open}
                    submit={(questions, course) => {
                        this.props.submitTest(questions, course)
                        window.location.reload();
                    }}
                />
            </div>
        )
    }
}