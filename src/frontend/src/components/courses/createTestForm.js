import React from 'react'
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap'


export default class CreateTestForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            questions: [" "],
            editing: false
        };
    }

    static getDerivedStateFromProps(props, current_state) {
        let t = props.course?props.course.test:false;
        if (!!t && !current_state.editing) {
            return {
                ...current_state,
                editing: true,
                questions: t.questions
            }
        } else {
            return {...current_state}
        }
    }

    render() {
        if (!this.props.course)
            return null;
        return (
            <div>
                <Modal isOpen={!!this.props.course}>
                    <ModalHeader toggle={this.props.close}>Тест</ModalHeader>
                    <ModalBody>
                        {this.state.questions.map((q, i) => (
                            <div className={"q-area"} key={i + "qi"}>
                                <div className={"title-main"}>
                                    Вопрос #{i + 1}
                                </div>
                                <textarea onChange={(e) => {
                                    let q = this.state.questions;
                                    q[i] = e.target.value;
                                    this.setState({...this.state, questions: q});
                                }} value={this.state.questions[i]} style={{width: "100%", margin:"2% 0"}}/>
                                <Button size={"sm"} color="danger"
                                        onClick={() => {
                                            let q = this.state.questions.slice(i, 1);
                                            this.setState({...this.state, questions: q});
                                        }}>
                                    Удалить
                                </Button>
                            </div>
                        ))
                        }
                    </ModalBody>
                    <ModalFooter>
                        <Button size={"sm"} color="primary"
                                onClick={() => {
                                    let q = this.state.questions;
                                    q.push(" ");
                                    this.setState({...this.state, questions: q});
                                }}>
                            Добавить
                        </Button>
                        <Button size={"sm"}  color="success"
                                onClick={() => {
                                    this.props.submit(this.state.questions, this.props.course)
                                }}>
                            Отправить
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>)
    }
}