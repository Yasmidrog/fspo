import React from 'react'
import {Form, FormGroup, Container, Row, Input, Label} from 'reactstrap'

export default class CourseForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            teacherIds: [],
            name: "",
            link: "",
            platform: "",
            desc: "",
            editingCourse: null
        };
        this.state.currentTeacher = "";
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();
        const s = this.state;
        if (!(!!s.desc && !!s.link && !!s.name))
            return;
        const course = {
            desc: s.desc,
            link: s.link,
            name: s.name,
            platform: s.platform,
            id: !!s.editingCourse ? s.editingCourse.course_id : undefined
        };
        if (s.teacherIds.length > 0)
            course.teachers = s.teacherIds;
        this.props.submitCourse(course);
        return true;
    }

    static getDerivedStateFromProps(props, current_state) {
        let c = props.editingCourse;
        if (!!c &&  !(current_state.editingCourse
            &&current_state.editingCourse.course_id===c.course_id)) {
            return {
                ...current_state,
                editingCourse: c,
                desc: c.desc,
                name: c.name,
                link: c.link,
                teacherIds:c.teachers?(c.teachers.reduce((acc, t)=>{
                 acc.push(t.teacher_id+'');
                    return acc;
                }, [])):[]
            }
        }else{
            console.log(current_state);
            return {...current_state}
        }
    }

    render() {
        return (
            <Container>
                <Row className={"justify-content-center"}>
                    <Form onSubmit={this.handleSubmit} id={"app-form"} className={"submit-form col-sm-6 col-xs-12"}>
                        {this.state.editingCourse && <div className={"text-muted"}>Редактирование курса {"  "}
                            <a href={this.state.editingCourse.link}>{this.state.editingCourse.name}</a> {"  "}
                            <span onClick={() => {
                                this.props.delEditCourse();
                                this.setState({...this.state, editingCourse: null, name:""})
                            }} className={"clickable-link"}>[x]</span>
                        </div>}
                        <FormGroup>
                            <Label for="teacher">Преподаватель</Label>
                            <Input onChange={(e) => {
                                this.setState({...this.state, currentTeacher: e.target.value})
                            }}
                                   value={this.state.currentTeacher}
                                   type="select" name="teacher" id="teacher">
                                <option key={"_t_id"} value={""}>Выберите преподавателя</option>
                                {!!this.props.teachers &&
                                this.props.teachers.map((t, i) => (
                                    <option value={t.user_id}
                                            key={t.user_id + "cc"}>
                                        {t.lastname + " " + t.firstname + " " + t.middlename}
                                    </option>
                                ))
                                }
                            </Input>


                            <div style={{paddingBottom: "10px"}} onClick={() => {
                                if (!this.state.currentTeacher)
                                    return;
                                let a = this.state.teacherIds;
                                a.push(this.state.currentTeacher);
                                this.setState({...this.state, teacherIds: a, currentTeacher: ""})
                            }} className={"clickable-link"}>
                                [добавить]
                            </div>
                            <div>
                                {!!this.props.teachers && this.props.teachers
                                    .filter((t) => this.state.teacherIds.includes(t.user_id))
                                    .map((t) =>
                                        <div className={"deletable"}>
                                            <div>{t.lastname + " " + t.firstname.charAt(0) + ". " + t.middlename.charAt(0) + "."}</div>
                                            <div onClick={() => {
                                                let tchs = this.state.teacherIds;
                                                let i;
                                                for (const [index, id] of tchs.entries())
                                                    if (id === t.user_id)
                                                        i = index;
                                                tchs.splice(i, 1);
                                                this.setState({...this.state, teacherIds: tchs})

                                            }} className={"delete-circle"}>
                                                <span className={"glyphicon glyphicon-remove"}></span>
                                            </div>
                                        </div>
                                    )
                                }
                            </div>
                        </FormGroup>

                        <FormGroup>
                            <Label for="link">Ссылка</Label>
                            <Input id={"link"} type={"link"}
                                   value={this.state.link}
                                   onChange={(e) =>
                                       this.setState({...this.state, link: e.target.value})}/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="c_name">Название</Label>
                            <Input id={"c_name"} type={"text"}
                                   value={this.state.name}

                                   onChange={(e) =>
                                       this.setState({...this.state, name: e.target.value})}/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="c_desc">Дополнительная информация</Label>
                            <Input id={"c_desc"}
                                   value={this.state.desc}
                                   type={"textarea"}
                                   placeholder={"-1 вопрос на экзамене"}
                                   onChange={(e) =>
                                       this.setState({...this.state, desc: e.target.value})}/>
                        </FormGroup>
                        <Input type={"submit"}/>
                        {!!this.props.error && this.props.error.message &&
                        <div className={"justify-content-center text-danger"}>{
                            this.props.error.message.replace("Validation error,", "")
                        }</div>}
                    </Form>
                </Row>

            </Container>)

    }
}