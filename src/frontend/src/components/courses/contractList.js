import React from 'react';
import {ListGroupItem, ListGroup} from 'reactstrap'

export default (props) => {

    return (<div className={"table-wrap"}>
        <ListGroup flush>
        {props.courses&& props.courses.map((c) => (
                    <ListGroupItem key={c.course_id+"contract"}>
                    <a href={c.link} target={"_blank"} className="alert-link">
                        {c.name}
                    </a><span>{c.enrolled.length}</span>
                    </ListGroupItem>
                ))
             }
        </ListGroup>
        </div>
    )
}