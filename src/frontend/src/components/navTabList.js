import classnames from "classnames";
import React from 'react';
import {

    Nav, NavItem, NavLink,
} from 'reactstrap';

export default (props) => {
    return (
        <Nav tabs>
            {props.names.map((n, i) => (
                <NavItem key={n.name + " " + i}>
                    <NavLink
                        className={classnames({active: props.activeTab === `${i + 1}`})}
                        onClick={() => {
                            props.toggle(`${i + 1}`);
                        }}
                    >
                        {n}
                    </NavLink>
                </NavItem>
            ))
            }
        </Nav>
    )
}