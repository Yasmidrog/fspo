import React from 'react';
import './styles/home.css'
import {Container, Row, TabContent, TabPane} from 'reactstrap';
import {ApplicationsList, ApproveList, StatsList, SubmitApplicationForm} from "./applications";
import TabList from './navTabList'
import Header from './header'
import ReactTooltip from 'react-tooltip'
import CourseTab from "./courses/courseTab"
import ContractList from "./courses/contractList"
import CourseForm from './courses/courseForm'
import Login from "./login"
import {bindActionCreators} from "redux";
import {
    clearMessage,
    delCourse,
    getApplicationsForMe,
    getMyApplications,
    loadCourses,
    setEditingCourse,
    submitAnswers,
    setEnrolled,
    submitTest,
    setStatus,
    submitApplication,
    submitCourse
} from "../modules/courses";
import {
    setTab
} from "../modules/index";
import {loadTeachers} from '../modules/teachers'
import {login, logout} from "../modules/login";
import connect from "react-redux/es/connect/connect";

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        if(this.props.activeTab==1){
            this.props.getMyApplications();

        }
    }


    // boolean to render list group items edge-to-edge in a parent container
    toggle(tab) {
        if (this.props.activeTab !== tab) {
            this.props.setTab(tab);
            switch (tab) {
                case "5":
                    this.props.loadTeachers();
                    break;
                case "1":
                case "2":
                case "3":
                    this.props.getMyApplications();
                    break;
                case "4":
                    this.props.loadCourses();
                    this.props.loadTeachers();
                    break;
                case "6":
                    this.props.loadCourses();
                    this.props.getApplicationsForMe();
                    break;
                case "7":
                    this.props.loadTeachers();
            }
        }
    }

    render() {

        let names = ["Главная",
            "Проверяемые",
            "Отклоненные",
            "Курсы",
            "Отправить сертификат",
        ];
        if (this.props.user) {
            if (this.props.user.roles.includes("admin")) {
                names.push("Администрирование");
                names.push("Добавить курс");
            }
            if (!this.props.user.roles.includes("admin") && this.props.user.roles.includes("teacher"))
                names.push("Администрирование");
        }
        ReactTooltip.rebuild();
        return (
            <Container fluid className="Home">
                <h2 className={"title-main"}> Система сертификации ФСПО ИТМО</h2>
                {this.props.user && <Header user={this.props.user} logout={this.props.logout}/>}
                <Row>
                    {this.props.isLoggedIn ?
                        <Container fluid>
                            <TabList activeTab={this.props.activeTab} toggle={(i) => this.toggle(i)}
                                     names={names}/>
                            <TabContent activeTab={this.props.activeTab}>
                                <TabPane key={"tab1"} tabId="1">
                                    <ApplicationsList submitAnswers={this.props.submitAnswers} applications={this.props.myApplications}/>
                                </TabPane>
                                <TabPane key={"tab2"} tabId="2" flush={"true"}>
                                    <ApplicationsList submitAnswers={this.props.submitAnswers} applications={this.props.myApplications} status={"pending"}/>
                                </TabPane>
                                <TabPane key={"tab3"} tabId="3" flush={"true"}>
                                    <ApplicationsList submitAnswers={this.props.submitAnswers} applications={this.props.myApplications} status={"denied"}/>
                                </TabPane>
                                <TabPane key={"tab4"} tabId="4" flush={"true"}>
                                    <CourseTab
                                        setEditCourse={(c) => {
                                            this.toggle("7");
                                            if (this.props.editingCourse && c.course_id === this.props.editingCourse.course_id)
                                                return;
                                            this.props.setEditingCourse(c);
                                        }}
                                        isAdmin={this.props.user.roles.includes("admin")}
                                        isTeacher={this.props.user.roles.includes("teacher")}
                                        submitTest={this.props.submitTest}
                                        setEnrolled={this.props.setEnrolled}
                                        delCourse={this.props.delCourse}
                                        courses={this.props.courses}
                                        teachers={this.props.teachers}/>
                                </TabPane>
                                <TabPane key={"tab5"} tabId="5" flush={"true"}>
                                    <SubmitApplicationForm clearMessage={this.props.clearMessage}
                                                           submitMessage={this.props.submitMessage}
                                                           submitApplication={this.props.submitApplication}
                                                           teachers={this.props.teachers}/>
                                </TabPane>
                                {(this.props.user.roles.includes("teacher") || this.props.user.roles.includes("admin")) &&
                                <TabPane style={{paddingTop: "40px"}} key={"tab6"} tabId="6" flush={"true"}>
                                    <h2>Подтверждение</h2>
                                    <ApproveList
                                        courses={this.props.courses}
                                        user={this.props.user}
                                        setStatus={this.props.setStatus}
                                        applications={!!this.props.allApps ? this.props.allApps.map((a) => {
                                            const r = {...a};
                                            if (!this.props.user.roles.includes("admin") && a.status_code === "moderation")
                                                r.blocked = true;
                                            return r;
                                        }).filter((a) => a.status_code === "pending" || a.status_code === "moderation") : null}/>
                                    <h2>Статистика</h2>
                                    <StatsList applications={this.props.allApps}/>
                                    <h2>Контракт</h2>
                                    <ContractList courses={this.props.courses}/>
                                </TabPane>
                                }
                                {this.props.user.roles.includes("admin") &&
                                <TabPane key={"tab7"} tabId="7" flush={"true"}>
                                    <CourseForm
                                        delEditCourse={() => this.props.setEditingCourse(undefined)}
                                        editingCourse={this.props.editingCourse}
                                        error={this.props.validationError}
                                        submitCourse={this.props.submitCourse}
                                        teachers={this.props.teachers}/>
                                </TabPane>
                                }

                            </TabContent>
                        </Container>
                        : <Login login={this.props.login} error={this.props.authError}/>}
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        courses: state.courses.courses,
        user: state.login.user,
        isLoggedIn: state.login.isLoggedIn,
        authError: state.errors.authError,
        teachers: state.teachers.teachers,
        myApplications: state.courses.myApplications,
        allApps: state.courses.allApps,
        submitMessage: state.courses.submitMessage,
        submitTest: state.courses.submitTest,
        submitAnswers:state.courses.submitAnswers,
        editingCourse: state.courses.editingCourse,
        error: state.errors.error,
        validationError: state.errors.validationError,
        activeTab:state.tabs.tab,
        setTab:state.setTab



    }
};


const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            getApplicationsForMe,
            setStatus,
            getMyApplications,
            loadTeachers,
            submitApplication,
            login,
            loadCourses,
            logout,
            submitAnswers,
            setTab,
            setEnrolled,
            submitCourse,
            delCourse,
            submitTest,
            clearMessage,
            setEditingCourse
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(Home);
