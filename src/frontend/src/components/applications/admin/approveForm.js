import React from "react";
import {Button, ModalHeader, ModalFooter, Modal, ModalBody, Label, Input} from "reactstrap";

export default class ApproveForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {course_id: "", comment: ""};
        this.setStatus = this.setStatus.bind(this);

    }
    static getDerivedStateFromProps(props, current_state) {
        if (!current_state.course_id&&props.app) {
            return {
                course_id:props.app.course_id,
                comment:props.app.comment,
            }
        }
        return null
    }
    setStatus(status) {
        this.props.app.status_code = status;
        this.props.setStatus(status, this.props.app, this.props.courses.filter(((c)=> c.course_id===this.state.course_id
        ))[0], this.state.comment)
    }

    render() {
        if (!this.props.isOpen)
            return null;
        return (
            <div>
                <Modal isOpen={this.props.isOpen}>
                    <ModalHeader toggle={this.props.close}>Заявка</ModalHeader>
                    <ModalBody>
                        {!!this.props.app.certificate_link && <div>
                            <a href={this.props.app.certificate_link} target={"_blank"}>
                                Ссылка на сертификат
                            </a>
                        </div>}
                        {!!this.props.app.picture &&
                        <div>
                            <a href={this.props.app.picture}>
                                Файл
                            </a>
                        </div>}
                        <div style={{marginTop:"5%"}}>
                            {this.props.user.roles.includes("admin")&&
                                <Input
                                    value={this.state.course_id}
                                    onChange={(e) => this.setState({
                                        ...this.state,
                                        course_id: e.target.value
                                    })}
                                    type="select">
                                    <option value={""} key={"nullcourse"}>Привяжите курс*</option>
                                    {!!this.props.courses &&
                                    this.props.courses.map((c, i) => (
                                        <option value={c.course_id}
                                                key={c.course_id + "sc"}>
                                            {c.name}
                                        </option>
                                    ))
                                    }
                                </Input>
                            }
                            <Label style={{marginTop:"3%"}} for="text">Комментарий**</Label>
                            <Input value={this.state.comment} type="textarea" name="text"
                                   onChange={(e)=>this.setState({...this.state, comment:e.target.value})} id="text" />
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <div>
                            <Button size={"sm"} disabled={!this.state.course_id} color="primary" onClick={() => {
                                this.props.close();
                                this.setStatus("approved")
                            }}>*подтв. препод.</Button>
                            {"  "}
                            {this.props.user.roles.includes("admin") &&
                            <Button size={"sm"} disabled={!this.state.course_id} color="primary" onClick={() => {
                                this.props.close();
                                this.setStatus("pending")
                            }}>*подтв. админ.
                            </Button>
                            }

                        </div>

                        <Button size={"sm"} disabled={!this.state.comment} color="danger" onClick={() => {
                            this.props.close();
                            this.setStatus("denied")
                        }}>
                            **Отклонить
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>)
    }
}