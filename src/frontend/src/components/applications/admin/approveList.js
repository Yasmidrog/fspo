import React from 'react';
import {
    Table
} from 'reactstrap';
import {ApproveForm} from '../index'
import InspectTest from "../inpectTest";
function  getDate(date){
    date=new Date(date);
    return ('0'+date.getMonth()).slice(-2)+ '/' +('0'+date.getDate()).slice(-2)+ '/' +
        ('0'+date.getFullYear()).slice(-2)+ ' ' +
        ('0'+date.getHours()).slice(-2)+':'+
        ('0'+date.getMinutes()).slice(-2);
}



export default class ApproveList extends React.Component{
    constructor(props){
        super(props);
        this.state={
            approveOpen:false,
            currentApplication:null,
            res_open:false
        };
        this.openApprove=this.openApprove.bind(this)
    }
    openApprove(app){
        this.setState({...this.state, approveOpen:!!app, currentApplication:app})
    }
    render() {
        let apps = this.props.applications;
        return (

            <div className={"table-wrap"}>
                <Table>
                    <thead>
                    <tr>
                        <th>Название
                        </th>
                        <th>
                            Автор
                        </th>
                        <th>Группа</th>
                        <th>Подтверждение</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        apps&&apps.map((app) => (
                            <tr key={app.application_id + "adm"}>
                                <th scope="row">
                                    <a href={app.certificate_link ? app.certificate_link : app.picture}
                                       target={"_blank"} className="alert-link">
                                        {app.course_name ? app.course_name : (app.certificate_link ? app.certificate_link : "Открыть изображение")}
                                    </a>
                                    <span className={"text-muted"}>{"    "+getDate(app.date)}</span>
                                    {app.test_result&& <span className={"clickable-link"} onClick={
                                        ()=>{
                                            this.setState({...this.state, res_open:app.test_result})
                                        }
                                    }> &nbsp; [Ответы на тест] </span>}
                                </th>
                                <td>{app.author.lastname + " " + app.author.firstname.charAt(0) + ". " + app.author.middlename.charAt(0) + "."}</td>
                                <td>{app.author.group || ""}</td>
                                <td>
                                    {app.blocked ?
                                        <div className={"text-muted"}>Проверка администратором</div>
                                        :
                                        <div className={"clickable-link"}
                                             onClick={() => this.openApprove(app)}>[Открыть]</div>
                                    }
                                </td>
                            </tr>
                        ))
                    }
                    </tbody>
                </Table>
               <ApproveForm
                   app={this.state.currentApplication}
                   courses={this.props.courses}
                            setStatus={this.props.setStatus}
                            close={()=>this.openApprove(null)}
                            isOpen={this.state.approveOpen}
                            user={this.props.user}/>

                <InspectTest
                    close={() => {
                        this.setState({...this.state, res_open: false})
                    }}
                    result={this.state.res_open}
                />
            </div>)
    }
}