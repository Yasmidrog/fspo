import React from "react";
import {Table, Row, Col, Badge, Input} from 'reactstrap'
import InspectTest from "../inpectTest";

export default class StatsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            student_filter: null,
            group_filter: "",
            course_filter: "",
            res_open:false
        };
        this.filter = this.filter.bind(this);
    }

    filter(applications) {
        let s = this.state;
        const groups = [];
        const students = [];
        const courses = [];
        applications.forEach((app) => {
            if (!!app.course_name&&!courses.includes(app.course_name))
                courses.push(app.course_name);
        });
        const byName = applications.reduce((acc, app) => {
            if (!app.course_name)
                return acc;
            if (!acc[app.course_name])
                acc[app.course_name] = [];
            if (!(s.course_filter && app.course_name !== s.course_filter ||
                s.group_filter && app.author.group !== s.group_filter||
                s.student_filter && app.author.id!= s.student_filter
            ))  acc[app.course_name].push(app);;



            let group = app.author.group;
            if (!!group&&!groups.includes(group))
                groups.push(group);
            let f = false;
            for (let s of students)
                if (app.author.id === s.id)
                    f = true;
            if (!f)
               if (!(s.group_filter && app.author.group !== s.group_filter))
                    students.push(app.author);

            return acc
        }, {});
        return {groups, students, courses, byName}
    }

    render() {
        let applications = this.props.applications;
        if (!applications)
            return (<div></div>);
        let {groups, students, courses, byName} = this.filter(applications);
        return (
            <div>
                <Row style={{marginBottom: "10px"}}>
                    <Col>
                        Курс:
                        <Input value={this.state.course_filter}
                               onChange={(e) => this.setState({...this.state, course_filter: e.target.value})}
                               type="select">
                            <option value={""}>Все</option>
                            {courses.map((name) => (
                                <option key={name + "cn"} value={name}>{name}</option>
                            ))}
                        </Input>
                    </Col>
                    <Col>
                        Группа:
                        <Input value={this.state.group_filter}
                               onChange={(e) => this.setState({...this.state, group_filter: e.target.value})}
                               type="select">
                            <option value={""}>Все</option>
                            {groups.map((g) => (<option key={g + "g"} value={g}>{g}</option>))}
                        </Input>
                    </Col>
                    <Col>
                        Студент:
                        <Input value={this.state.student_filter}
                               onChange={(e) => this.setState({...this.state, student_filter: e.target.value})}
                               type="select">
                            <option value={""}>Все</option>
                            {students.map((s) => (<option key={s.id + "filter"}
                                                          value={s.id}>{s.lastname + " " + s.firstname}</option>))}

                        </Input>
                    </Col>
                </Row>
                <div className={"table-wrap"}>
                    {Object.keys(byName)
                        .filter((n)=>!this.state.course_filter||this.state.course_filter===n)
                        .map((name) => {
                        const apps = byName[name];
                        return (
                            <Table>
                                <thead>
                                <tr>
                                    <th style={{width: "45%"}}>
                                        <a href="http://example.com" target={"_blank"} className="alert-link">
                                            {name}
                                        </a></th>
                                    <th className="text-right">Группа</th>
                                    <th className="text-right">Админ</th>
                                    <th className="text-right">Преподаватель</th>
                                </tr>
                                </thead>
                                <tbody>
                                {apps.map(a => (
                                    <tr>
                                        <td>
                                            {a.author.lastname + ' ' + a.author.firstname.substr(0, 1) + ". " + a.author.middlename.substr(0, 1) + '.'}
                                            {a.test_result&& <span onClick={
                                                ()=>{
                                                    this.setState({...this.state, res_open:a.test_result})
                                                }
                                            } className={"clickable-link"}> &nbsp; [Ответы на тест] </span>}
                                        </td>
                                        <td className="text-right">{a.author.group || ""}</td>
                                        <td className="text-right">
                                            <Badge
                                                color={a.status_code === "moderation" ? "secondary" : (a.status_code === "pending" || a.status_code === "approved")
                                                    ? "success" : "danger"}>
                                            <span
                                                className={"glyphicon glyphicon-" +
                                                (a.status_code === "moderation" ? "time" :
                                                    ((a.status_code === "pending" || a.status_code === "approved") ? "ok" : "remove"))}></span>
                                            </Badge>
                                        </td>
                                        <td className="text-right">
                                            <Badge
                                                color={(a.status_code === "pending" || a.status_code === "moderation")
                                                    ? "secondary" : (a.status_code === "approved" ? "success" : "danger")}>
                                    <span className={"glyphicon glyphicon-" +
                                    ((a.status_code === "pending" || a.status_code === "moderation")
                                        ? "time"
                                        : (a.status_code === "approved" ? "ok" : "remove"))}></span>
                                            </Badge>
                                        </td>
                                    </tr>
                                ))
                                }
                                </tbody>
                            </Table>
                        )
                    })
                    }
                </div>
                <InspectTest
                    close={() => {
                        this.setState({...this.state, res_open: false})
                    }}
                    result={this.state.res_open}
                />
            </div>)
    }
}