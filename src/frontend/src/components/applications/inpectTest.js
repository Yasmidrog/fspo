import React from 'react'
import {Button, Modal, ModalBody, ModalHeader} from 'reactstrap'


export default class InspectTest extends React.Component {

    constructor(props) {
        super(props);

    }
    render() {
        if (!this.props.result)
            return null;
        return (
            <div>
                <Modal isOpen={!!this.props.result}>
                    <ModalHeader toggle={this.props.close}>Ответы</ModalHeader>
                    <ModalBody>
                        {this.props.result&&this.props.result.answers.map((a, i) => (
                            <div className={"q-area"} key={i + "qi"}>
                                <div style={{fontSize:"1.5rem", paddingBottom:"2%"}}>
                                    Вопрос: {a.question}
                                </div>
                                <div style={{fontSize:"1rem", paddingBottom:"2%"}} className={""}>
                                    {a.answer}
                                </div>
                            </div>
                        ))
                        }
                    </ModalBody>
                </Modal>
            </div>)
    }
}