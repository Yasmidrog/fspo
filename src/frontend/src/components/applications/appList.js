import React from 'react';
import {ListGroup, ListGroupItem} from 'reactstrap';
import ApplicationRow from "./applicationRow"
import ReactTooltip from 'react-tooltip'
import AnswerTest from "./answerTest";
import InspectTest from "./inpectTest";


export default class AppList extends React.Component {
    constructor(props) {

        super(props);
        this.state = {
            res_open: false,
            test_open: false
        };
    }


    render() {
        let props = this.props;
        let apps = props.applications;
        if (apps && props.status) {
            apps = apps.filter((a) => {
                switch (props.status) {
                    case "pending":
                        return a.status_code === "moderation" || a.status_code === "pending";
                    case "denied":
                        return a.status_code === "denied"
                }
            })
        }

        return (
            <div>
                <ListGroup flush>
                    {apps && apps.map(a => {
                        return (
                            <ListGroupItem key={a.application_id}>
                                <ApplicationRow answerTest={() => {
                                    this.setState({...this.state, test_open: a})
                                }} openResult={() => {
                                    if (a.test_result)
                                        this.setState({...this.state, res_open: a.test_result})
                                }}
                                                application={a}/>{"  "}
                            </ListGroupItem>

                        )
                    })
                    }
                    <ReactTooltip/>
                </ListGroup>

                <AnswerTest
                    close={() => {
                        this.setState({...this.state, test_open: false})
                    }}
                    application={this.state.test_open}
                    submit={(answers, app) => {
                        this.props.submitAnswers(answers, app);
                        window.location.reload();
                    }}
                />

                <InspectTest
                    close={() => {
                        this.setState({...this.state, res_open: false})
                    }}
                    result={this.state.res_open}
                />
            </div>


        )
    }

}

