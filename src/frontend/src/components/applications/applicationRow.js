import {Badge} from "reactstrap";
import React from "react";
function  getDate(date){
    date=new Date(date);
    return ('0'+date.getMonth()).slice(-2)+ '/' +('0'+date.getDate()).slice(-2)+ '/' +
        ('0'+date.getFullYear()).slice(-2)+ ' ' +
        ('0'+date.getHours()).slice(-2)+':'+
        ('0'+date.getMinutes()).slice(-2);
}
export default (props) => {

    const app = props.application;
    return (<div>
        <a href={app.certificate_link ? app.certificate_link : app.picture} target={"_blank"} className="alert-link">
            {app.course_name ? app.course_name : (app.certificate_link ? app.certificate_link : "Открыть изображение")}
        </a>
        {(app.status_code === "moderation" ?
                <span>
                            <Badge color={"secondary"}>
                                  <span className={"glyphicon glyphicon-time"}></span>
                            </Badge>
                            <Badge color={"secondary"}><span className={"glyphicon glyphicon-time"}></span></Badge>
                        </span>

                :
                <span>
                              <Badge color={(app.status_code==="pending"||app.status_code==="approved")?"success":"danger"}>
                                  <span className={"glyphicon glyphicon-"+(app.status_code==="pending"||app.status_code==="approved"
                                      ?"ok":"remove")}></span></Badge>
                    {app.status_code === "pending" ?
                        <Badge color={"secondary"}><span className={"glyphicon glyphicon-time"}></span></Badge> :
                        <span>
                                <Badge color={app.status_code === "approved" ? "success" : "danger"}>
                                    <span
                                        className={"glyphicon glyphicon-" + (app.status_code === "approved" ? "ok" : "remove")}>
                                    </span>
                              </Badge>
                                    </span>
                    }
                        </span>
        )}
        <span className={"text-muted"}>{"   "+getDate(app.date)+"   "}</span>
        {app.comment&& <span className={"text-muted"} data-tip={app.comment}> &nbsp; коммент.</span>}
        {app.test&& <span onClick={
            ()=>props.answerTest()
        } className={"clickable-link"}> &nbsp; {`[П${app.test_result?"ереп":""}ройти тест]`} </span>}
        {app.test_result&& <span onClick={
            ()=>props.openResult()
        } className={"clickable-link"}> &nbsp; [Ответы на тест] </span>}

    </div>)
}