import React from 'react'
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap'


export default class AnswerTest extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            answers: [],
            init: false
        };
    }

    static getDerivedStateFromProps(props, current_state) {
        if (!current_state.init && !!props.application && !!props.application.test) {
            return {
                ...current_state,
                init: true,
                answers: props.application.test.questions.map((q) => {
                    return {question: q, answer: ""}
                })
            }
        } else {
            return {...current_state}
        }
    }

    render() {
        if (!this.props.application&&!this.props.application.test)
            return null;
        return (
            <div>
                <Modal isOpen={!!this.props.application}>
                    <ModalHeader toggle={this.props.close}>Тест</ModalHeader>
                    <ModalBody>
                        {this.state.answers.map((q, i) => (
                            <div className={"q-area"} key={i + "qi"}>
                                <div className={""}>
                                    {q.question}
                                </div>
                                <textarea onChange={(e) => {
                                    let q = this.state.answers;
                                    q[i].answer = e.target.value;
                                    this.setState({...this.state, answers: q});
                                }} value={this.state.answers[i].answer} style={{width: "100%", margin: "2% 0"}}/>
                            </div>
                        ))
                        }
                    </ModalBody>
                    <ModalFooter>
                        <Button size={"sm"} color="success"
                                onClick={() => {
                                    this.props.submit(this.state.answers, this.props.application)
                                }}>
                            Отправить
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>)
    }
}