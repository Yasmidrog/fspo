import React from 'react';
import {
    Label, Row, Container, Input,
    ModalBody, ModalFooter, Button
    , Modal, Form, FormGroup
} from 'reactstrap'

export default class SubmitAppForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            teacher: "",
            file: null,
            link: "",
            modal: false,
            confirmed: false
        };
        this.toggle = this.toggle.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleSubmit(e) {
        e.preventDefault();
        if (!this.state.teacher && !this.state.confirmed) {
            this.toggle();
        } else if (this.state.file || this.state.link) {
            this.props.clearMessage();
            this.props.submitApplication(this.state.teacher, this.state.link, this.state.file)

        }

    }

    toggle() {
        this.setState({
            ...this.state,
            modal: !this.state.modal
        });
    }

    render() {
        return (
            <Container fluid>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalBody>
                        Вы точно проходили курс самостоятельно?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={() => {
                            this.setState({...this.state, confirmed: true,   modal: false});
                            this.props.clearMessage();
                            this.props.submitApplication(this.state.teacher, this.state.link, this.state.file)
                        }}>Да</Button>{' '}
                        <Button color="secondary" onClick={this.toggle}>Нет, вернуться</Button>
                    </ModalFooter>
                </Modal>

                <Row className={"justify-content-center"}>
                    <Form id={"app-form"} onSubmit={this.handleSubmit} className={"submit-form col-sm-4 col-xs-12"}>
                        <FormGroup>
                            <Label for="teacher">Преподаватель</Label>
                            <Input onChange={(e) => {
                                this.setState({...this.state, teacher: e.target.value})
                            }}
                                   value={this.state.teacher}
                                   type="select" name="teacher" id="teacher">
                                <option key={"none_t_id"} value={""}>Самостоятельно</option>
                                {!!this.props.teachers &&
                                this.props.teachers.map((t, i) => (
                                    <option value={t.user_id}
                                            key={t.user_id}>
                                        {t.lastname + " " + t.firstname + " " + t.middlename}
                                    </option>
                                ))
                                }
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label for="link">Ссылка</Label>
                            <Input id={"link"} type={"link"}
                                   onChange={(e) =>
                                       this.setState({...this.state, link: e.target.value})}/>
                        </FormGroup>
                        <div className={"or"}>Или</div>
                        <FormGroup>
                            <Input type={"file"} accept={"image/*, application/pdf"}
                                   onChange={(e) =>
                                       this.setState({...this.state, file: e.target.files[0]})}/>
                        </FormGroup>
                        <Input type={"submit"} disabled={!(this.state.file || this.state.link)}/>
                        {!!this.props.submitMessage &&
                        <div className={"justify-content-center text-warning text-big"}>{
                            this.props.submitMessage
                        }</div>}
                    </Form>
                </Row>
            </Container>
        )
    }
}