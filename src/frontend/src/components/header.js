import {Col, Row} from 'reactstrap'
import React from "react";

export default props => (

    <Row className="Home-header">
        <img src={props.user.photo} className="Home-logo"
             alt="logo"/>
        <Col>
            <h2>
                {props.user.firstname+" "+props.user.lastname+(props.user.group?(", "+props.user.group):'')}
            </h2>
            <p>&nbsp;<span className={"glyphicon glyphicon-envelope"}></span>&nbsp;&nbsp;{props.user.email}</p>
            <p>&nbsp;<span className={"glyphicon glyphicon-earphone"}></span>&nbsp;&nbsp;{props.user.phone}</p>
            <p>&nbsp;<span className={"clickable-link medium text-muted"} onClick={props.logout}>Выход</span></p>
        </Col>
    </Row>)