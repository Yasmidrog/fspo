import React from 'react';
import {Form, FormGroup, Input, Container, Row} from 'reactstrap'
const getError=(mes)=>{
    switch (mes) {
        case "Wrong credentials":
            return "Неправильный логин или пароль"
        default:
            return mes;

    }
};


class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state =
            {
              username: null,
              password: null
            };
    }
    handleChange(e){
        this.setState({...this.state, [e.target.name]:e.target.value});
    }
    handleSubmit(e){
        e.preventDefault();
        const s=this.state;
        if(s.username&&s.password){
            this.props.login(s.username, s.password)
        }
    }
    render(){
        return (
            <Container fluid>
                <Row className={"justify-content-center"}>

                <Form  className={"submit-form col-sm-5 col-xs-12"} onSubmit={this.handleSubmit.bind(this)}>
                   <FormGroup> <Input type="text" name={"username"}
                           onChange={this.handleChange.bind(this)}/>
                   </FormGroup>
                    <FormGroup>
                    <Input type="password" name={"password"}
                           onChange={this.handleChange.bind(this)}/>
                    </FormGroup>
                    <Input type={"submit"} value={"Войти"}/>
                </Form>
                </Row>
                {this.props.error&&<Row className={"justify-content-center text-danger"}>{
                    getError(this.props.error.message)
                }</Row>}
            </Container>

        )
    }
}

export default Login;
