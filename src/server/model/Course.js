const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const errors = require("../config/errors");
const api = require("../../server/common/API/fspoRequester");
const Application = require("./Applicaton");
const Test = require("./Tests").Test;


const Course = new mongoose.Schema({
    name: {type: String, unique:true, required: true},
    link: {
        type: String, required: true,
        validate: {
            validator: function (v) {
                return /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi.test(v);
            },
            message: '{VALUE} is not a valid link!'
        }
    },
    desc: String,
    teacher_ids: [ Number],
    platform: {type:String},

    enrolledStudents:[Number]
}, { usePushEach: true });

Course.plugin(uniqueValidator);
Course.statics.addCourse = async function (params) {
    const c = this.model("Course")({link: params.link, name: params.name, desc: params.desc || "", enrolledStudents: []});
    return await saveNew(c, params)
};
Course.methods.addEnrolled=async function(student_id){
  this.enrolledStudents.push(student_id);
  console.log(this.enrolledStudents)
  return await this.save()
};
Course.methods.removeEnrolled=async function(student_id){
    const i= this.enrolledStudents.indexOf(student_id);
    if (i> -1)
        this.enrolledStudents.splice(i, 1);
    return await this.save()
};
async function saveNew(c, params){
    c.teacher_ids=[];

    if(params.teachers&&params.teachers.length>0) {
        for (let t of params.teachers) {
            let i = parseInt(t);
            if (i&&!c.teacher_ids.includes(i))
                c.teacher_ids.push(i);
        }
    }
    console.log(c);
    const e = c.validateSync();

    if (e) throw errors.generateErr(400, "Validation error, Некорректная ссылка");
    return  await c.save()
}
Course.methods.edit= async function(params){
    this.link=params.link;
    this.desc=params.desc||'';
    this.name=params.name;
    return await saveNew(this, params)
};
Course.methods.getTeachersNames = async function (token) {
    return await Promise.all(this.teacher_ids.map(
        async id => Object.assign(await api.getUserInfo(id, token), {teacher_id:id})));
};
Course.methods.getPending=async function(token, id, origin, admin){
    let q=admin?{
        course: this._id,
    }:{
        course: this._id,
        teacher_id: id,
        status:"pending"
    };
    const applications= await Promise.all(
        (    (await Application.find(q).sort({status: 1, created_at: -1}).populate("course")))
            .map(async app => await app.getPopulatedData(token, origin)));

   return {
       count:applications.length,
       applications
   }

};


Course.methods.findTest=async function(teacher_id){
    return await Test.findOne({teacher_id, course_id: this._id });
};
module.exports = mongoose.model("Course", Course);