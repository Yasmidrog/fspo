const mongoose = require('mongoose');
const api = require("../../server/common/API/fspoRequester");
const statuses = require("../config/config").statuses;
const Tests = require("./Tests");
const Application = new mongoose.Schema({
    status: {
        type: String,
        required: true,
        default: 'moderation',
        enum: ['moderation', 'pending', 'approved', 'denied']
    },
    teacher_id: {type: Number},
    course: {type: mongoose.Schema.Types.ObjectId, ref: "Course"},
    author_id: {type: Number, required: true},
    file: String,
    link: String,
    comment: String,
}, {timestamps: {createdAt: 'created_at', updatedAt: "updated_at"}});

Application.methods.getPopulatedData = async function (token, url) {
    const obj = {};

    const author = await api.getUserInfo(this.author_id, token);
    obj.author = Object.assign({id: this.author_id}, author);
    if (this.teacher_id) {
        const teacher = await api.getUserInfo(this.teacher_id, token);
        obj.teacher = Object.assign({id: this.teacher_id}, teacher);
    }
    obj.date = this.created_at;
    if (this.file)
        obj.picture = url + this._id + "/picture";


    if (this.course) {
        obj.course_link = this.course.link;
        obj.course_name = this.course.name;
        obj.course_id = this.course._id;
    }
    if (this.comment)
        obj.comment = this.comment;
    obj.certificate_link = this.link;
    obj.status = statuses[this.status];
    obj.status_code = this.status;
    obj.application_id = this._id;
    obj.date = this.created_at;
    if (obj.course_id && this.teacher_id) {
        let test = await Tests.Test.findOne({course_id: obj.course_id, teacher_id: this.teacher_id});
        if (!!test) {
            obj.test = test;
        }
    }

    console.log(obj.test);
    if (!!obj.test) {
        let res = await Tests.TestResult.findOne({author_id: this.author_id, test: obj.test._id}).populate("test");

        if (!!res) {
            obj.test_result = res;
        }
    }
    return obj;
};
module.exports = mongoose.model("Application", Application);