const mongoose = require('mongoose');
const Admin = new mongoose.Schema({
    fspo_id: {type: Number, unique: true, required: true},
});

module.exports = mongoose.model("Admin", Admin);