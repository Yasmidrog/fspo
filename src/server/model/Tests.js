const mongoose = require('mongoose');

const Test = new mongoose.Schema({
    questions: [String],
    teacher_id: Number,
    course_id: {type: mongoose.Schema.Types.ObjectId, ref: "Course"}
});

const TestResult = new mongoose.Schema({
    answers: [{question: String, answer: String}],
    test: {type: mongoose.Schema.Types.ObjectId, ref: "Test"},
    author_id: Number
});



module.exports.TestResult =  mongoose.model("TestResult", TestResult);
module.exports.Test =  mongoose.model("Test", Test);