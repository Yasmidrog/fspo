
module.exports.check_auth = function (roles) {
    if (!roles)
        roles = ["student", "admin", "teacher"];
    return async function (ctx, next) {
        if (ctx.state.user) {
            if(ctx.state.user.roles.includes("admin"))
                return await next();
            for (let r of roles)
                if (ctx.state.user.roles.includes(r))
                    return await next();
            return ctx.throw(ctx.errorsList.forbidden);
        } else {
            ctx.throw(ctx.errorsList.not_autorized);
        }
    }
};