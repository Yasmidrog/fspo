const api = require("../API/fspoRequester");

const Admin = require("../../model/Admin");
const jwt = require("jsonwebtoken");

module.exports.login = async function (username, password, secret) {
    console.log("userauth "+username+" "+password);
    const res = await api.requestAuth(username, password);
    const user = await api.getUserData(res.user_id, res.token);
    if (!!await Admin.findOne({fspo_id: user.fspo_id}))
        user.roles.push("admin");
    const payload = {
        fspo_token: user.token,
        fspo_id: user.fspo_id
    };
    const jwt_token = jwt.sign(payload, secret, {
        expiresIn: "24h"
    });
    return {user, jwt_token}
};