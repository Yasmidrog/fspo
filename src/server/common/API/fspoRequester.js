const fetch = require("node-fetch");
const URLparams = require("url").URLSearchParams;
const conf = require("../../config/config");
const errors = require("../../config/errors");
const app_key = conf.app_key;
const api_hostname = conf.api_hostname;

async function handleFspoResponse(url, req) {

    const res = await fetch(url, req).then((res)=>res.json());
    if (res.result === "error") {
        console.log(url, req, res);
        switch (res.error_code) {
            case 6:
                throw errors.list.wrong_credentials;
            case 8:
                throw  errors.list.invalid_token;

            default:
                throw errors.generateErr(400, res.message);
        }
    } else {
        delete res.result;
        return res;
    }
}

module.exports.requestAuth = async function (login, password) {
    const params = new URLparams;
    params.append('jsondata', JSON.stringify({
        login: login,
        password: password,
        app_key: app_key
    }));

    return await handleFspoResponse(api_hostname + '/authorization', {method: 'POST', body: params});
};

module.exports.getUserData = async function (uid, token) {
    let res;
    res = await handleFspoResponse(api_hostname + `/roles?user_id=${uid}`, {
        method: 'GET',
        headers: {token}
    });
    const roles = [];
    if (res.teacher)
        roles.push("teacher");
    if (res.student)
        roles.push("student");
    res = await handleFspoResponse(api_hostname + `/profile?user_id=${uid}`,
        {
            method: 'GET',
            headers: {token},

        });
    let inf= {
        roles,
        firstname: res.firstname,
        lastname: res.lastname,
        middlename: res.middlename,
        email: res.email,
        fspo_id: uid,
        token,
        phone:res.phone,
        photo:res.photo
    };
    let group=await getGroup(uid, token);
    if(group)
        inf.group=group;
    return inf;

};
module.exports.getUserInfo = async function (uid, token) {
    const res = await handleFspoResponse(api_hostname + `/profile?user_id=${uid}`, {
        method: 'GET',
        headers: {token},
    });
    let inf ={
        firstname: res.firstname,
        lastname: res.lastname,
        middlename: res.middlename,

    };
   let group=await getGroup(uid, token);
    if(!!group)
        inf.group=group;
   return inf;

};

async function getGroup(uid, token) {
    try {
        const groups = await handleFspoResponse(api_hostname + `/studentHistory?user_id=${uid}`, {
            method: 'GET',
            headers: {token},
        });
        if(groups.status==0)
         for (let g of groups.groups) {
            if (g.years.includes((new Date()).getFullYear())&&g.years.includes((new Date()).getFullYear()+1))
                return g.name;
         }
    }catch (e) {
        return false
    }
}

module.exports.getTeachers = async function (uid, token) {
    let res;
    const teachers = [];

    res = await handleFspoResponse(api_hostname + `/teachers?app_key=` + app_key, {
        method: 'GET',
        headers: {token}
    });
    for (let t of res.teachers) {
        teachers.push((({user_id, lastname, firstname, middlename}) =>
            ({user_id, lastname, firstname, middlename}))(t));
    }
    return teachers
};
module.exports.getGroups = async function (uid, token) {
    let res;
    res = await handleFspoResponse(api_hostname + `/groups?app_key=` + app_key, {
        method: 'GET',
        headers: {token}
    });
    const groups=[];
    for(let c of res.courses){
        groups.push(...c.groups.map(g=>g.name));
    }
    return groups;
};