const Koa        = require('koa');

const app = new Koa(); // admin app

app.use(require("./routes/courses"));

module.exports = app;