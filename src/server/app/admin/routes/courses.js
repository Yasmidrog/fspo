const Router  = require('koa-router');
const courses=require("../controllers/courses");
router = new Router({prefix:"/admin"});
router.post("/courses/new",courses.newCourse);
router.get("/courses/:id/delete",courses.delCourse);
module.exports=router.routes();