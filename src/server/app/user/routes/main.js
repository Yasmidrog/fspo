const Router  = require('koa-router');
let router = new Router();
const main=require("../controller/main");
const auth=require("../../../common/auth/auth").check_auth;

router.post("/login", main.login);
router.get('/teachers',auth(), main.getTeachers);
router.get("/groups", main.getGroups);
module.exports=router.routes();