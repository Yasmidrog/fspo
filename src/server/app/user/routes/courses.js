const Router  = require('koa-router');
let router = new Router();
const courses=require("../controller/courses");
const tests=require("../controller/tests");

const auth=require("../../../common/auth/auth").check_auth;
router.get('/applications', auth(), courses.getApplications);
router.get('/application/:id/picture', courses.getPic);
router.get('/courses', auth(), courses.getAllCourses);
router.post("/applications/new", auth(), courses.createOrUpdateAppliction);
router.post("/applications/:id/update", auth(), courses.createOrUpdateAppliction);
router.post("/applications/setStatus", auth("teacher"), courses.setStatus);
router.post("/enroll", auth(), courses.setEnrolled);
router.post("/tests/new", auth("teacher"), tests.createTest);
router.post("/tests/ans", auth(), tests.answerTest);



module.exports=router.routes();
