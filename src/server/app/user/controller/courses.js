const Application = require("../../../model/Applicaton");
const Course = require("../../../model/Course");
const path = require("path");
const send = require("koa-send");
const errors = require("../../../config/errors");
module.exports.getApplications = async function (ctx) {
    const q = ctx.query;
    const params = {};
    const uid = ctx.state.user.fspo_id;
    if (!q.type || q.type === "my") {
        params.author_id = uid;
    } else if (q.type === "forme") {
        if (ctx.checkRole("teacher") && !ctx.checkRole("admin")) {
            params.teacher_id = uid;
        }
        else if (ctx.checkRole("student") && !ctx.checkRole("admin"))
            throw errors.list.forbidden;
    } else {
        throw errors.list.wrong_app_type;
    }

    const apps = await Promise.all(
        (await Application.find(params).sort({status: 1, created_at: -1}).populate("course"))
            .map(async app =>
                await app.getPopulatedData(ctx.state.user.token,
                    ctx.request.origin + "/application/"))
    );
    apps.sort((a, b) => a.date > b.date ? 1 : -1);
    ctx.body = {
        status: 200,
        applications: apps
    }

};
module.exports.getPic = async function (ctx) {
    const f = (await Application.findById(ctx.params.id)).file;
    if (f)
        await send(ctx, path.relative("", f), {root: process.cwd()});
    else ctx.throw(ctx.errorsList.no_file)
};

module.exports.createOrUpdateAppliction = async function (ctx) {
    let f = ctx.request.body.files;
    let b = ctx.request.body.fields || ctx.request.body;
    if (!b)
        ctx.throw(ctx.errorsList.bad_request);
    let ap;
    if (ctx.request.params && ctx.request.params.id) {
        ap = await Application.findById(ctx.request.params.id);
        if (!ap)
            return ctx.throw(404)
    } else
        ap = new Application();

    if (f && f.file) ap.file = f.file.path;

    ap.author_id = ctx.state.user.fspo_id || ap.author_id;
    ap.link = b.link || ap.link;
    if (b.teacher_id)
        ap.teacher_id = b.teacher_id;
    if (b.course_id)
        ap.course = b.course_id;
    await ap.save();
    ctx.body = {
        status: 200
    }
};
module.exports.setStatus = async function (ctx) {
    //todo подтверждение своих
    const app = await Application.findById(ctx.request.body.application_id);
    const status = ctx.request.body.status;
    if (!(!!app && !!status))
        return ctx.throw(ctx.errorsList.bad_request);
    if (!(ctx.checkRole("admin") || (ctx.checkRole("teacher"))))
        return ctx.throw(ctx.errorsList.forbidden);
    if (!ctx.checkRole("admin") && (app.status === "moderation") && status !== "moderation")
        return ctx.throw(ctx.errorsList.forbidden);
    if (!["approved", "denied", "pending", "moderation"].includes(status))
        return ctx.throw(ctx.errorsList.invalid_status);
    app.status = status;
    if (ctx.request.body.course_id && ctx.checkRole("admin"))
        app.course = ctx.request.body.course_id;
    if (ctx.request.body.comment)
        app.comment = ctx.request.body.comment;
    await app.save();
    ctx.body = {
        status: 200
    }
};
module.exports.setEnrolled = async function (ctx) {
    const course = await Course.findById(ctx.request.body.course_id);

    if (!course)
        return ctx.throw(404);   //останавиливаем текущую функцию, сждем выполнения всех остальных,
    // продолжаем с уже полученными ошибками
    else if (!!ctx.request.body.enroll)
        await course.addEnrolled(ctx.state.user.fspo_id);
    else
        await course.removeEnrolled(ctx.state.user.fspo_id);

    ctx.body = {
        status: 200
    }
};
module.exports.getAllCourses = async function (ctx) {
    const crs = await Course.find({});
    ctx.body = {
        status: 200,
        courses: await Promise.all(crs.filter((c) => {
            if (ctx.query.by_teacher === "true")
                return c.teacher_ids.includes(parseInt(ctx.state.user.fspo_id + ""));
            return true;
        }).map(async (course) => {
            const c = {};
            c.desc = course.desc;
            c.link = course.link;
            c.course_id = course._id;
            c.name = course.name;
            if (ctx.state.user)
                c.status = course.enrolledStudents.includes(ctx.state.user.fspo_id) ? "enrolled" : "not_enrolled";
            c.enrolled = course.enrolledStudents;
            c.teachers = await course.getTeachersNames(ctx.state.user.token);
            let test = await course.findTest(ctx.state.user.fspo_id);
            if (!!test) c.test = test;
            if (ctx.checkRole("teacher") || ctx.checkRole("admin")) {
                c.pending_apps = await course.getPending(ctx.state.user.token, ctx.state.user.fspo_id,
                    ctx.request.origin + "/application/")
            }
            return c;
        }))
    };
};