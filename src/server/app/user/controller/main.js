const api=require("../../../common/API/fspoRequester");
const login=require("../../../common/auth/passport").login;
module.exports.getTeachers=async function (ctx) {
    ctx.body={status:200,  teachers: await api.getTeachers() }
};
module.exports.login= async function(ctx) {
    const res= await login(ctx.request.body.username, ctx.request.body.password, ctx.secret);
    ctx.state.user=Object.assign({}, res.user);
    delete res.user.token;
    ctx.body={status:200, user:res.user, jwt:res.jwt_token}
};
module.exports.getGroups = async function (ctx) {
    ctx.body = {status: 200, groups: await api.getGroups()}
};
