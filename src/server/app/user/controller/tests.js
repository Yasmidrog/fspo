const Course = require("../../../model/Course");
const Tests = require("../../../model/Tests");
module.exports.createTest = async function (ctx) {
    const course = await Course.findById(ctx.request.body.course_id);
    if (!course)
        return ctx.throw(404);
    const questions = ctx.request.body.questions;
    if (!Array.isArray(questions))
        return ctx.throw(400);
    let test = (!!ctx.request.body.test_id ?
        await Tests.Test.findOne({_id: ctx.request.body.test_id}) :
        new Tests.Test()) || new Tests.Test();
    test.questions = questions;
    test.course_id = course._id;
    test.teacher_id = ctx.state.user.fspo_id;
    await test.save();
    ctx.body = {
        status: 200
    }

};

module.exports.answerTest = async function (ctx) {
    const test = await Tests.Test.findById(ctx.request.body.test_id);

    if (!test)
        return ctx.throw(404);
    const answers = [];
    for (let a of ctx.request.body.answers) {
        if(!!a.question)
            answers.push({question: a.question, answer: a.answer||""});
    }
    let result = (!!ctx.request.body.res_id ?
        await Tests.TestResult.findOne({_id: ctx.request.body.res_id, author_id: ctx.state.user.fspo_id}) :
        new Tests.TestResult()) || new Tests.TestResult();

    result.answers = answers;
    result.test = test._id;

    result.author_id = ctx.state.user.fspo_id;
    await result.save();
    ctx.body = {
        status: 200
    }
};