const koa = require("koa");
const mongoose = require('mongoose');
const cors = require('@koa/cors');
const body = require('koa-body');
const compose = require('koa-compose');
const compress = require('koa-compress');
const session = require('koa-session');
const logger = require('koa-logger');

const MongooseStore = require('koa-session-mongoose');
const app = new koa();
app.context.errorsList = require("./config/errors").list;
app.context.checkRole = function (role) {
    return !!this.state.user && this.state.user.roles.includes(role)
};
/*
переопределяем стандартные промисы Монго
 */
mongoose.Promise = require('bluebird');
mongoose
    .connect(require("./config/dbconf").url)
    .then(() => {
        console.log('mongo connection created')
    })
    .catch((err) => {
        console.log("Error connecting to Mongo");
        console.log(err);
    });
/*
Время ответа
 */
app.use(logger());
if (process.env.NODE_ENV !== "production") {
    app.use(cors());

} else {
    let serve = require('koa-static');
    const path = require("path");
    app.use(serve(path.resolve(__dirname, '..', '..', 'build')));
}
app.use(async function responseTime(ctx, next) {
    const t1 = Date.now();
    await next();
    const t2 = Date.now();
    ctx.set('X-Response-Time', Math.ceil(t2 - t1) + 'ms');

});
/*
Коа ловит ошибки в цепочке промисов middleware, поэтому размещаем в начале
 */
app.use(async function handleErrors(ctx, next) {
    try {
        //останавиливаем текущую функцию, сждем выполнения всех остальных,
        // продолжаем с уже полученными ошибками
        await next();
        if (ctx.status === 404) {
            ctx.throw(404, "Not found")
        }
    } catch (e) {
        if (!e.status)
            e.status = 500;
        if(e.status===500&&e.message.includes("`name` to be unique")){
            e.status=400;
            e.message="Validation error, Курс с таким именем уже есть";
        }
        if (e.status === 404)
            e.message = "Not found";
        //if (e.status === 500 || process.env.NODE_ENV !== "production")
        ctx.app.emit('error', e, ctx);
        ctx.response.status = e.status;
        ctx.body = {
            status: e.status,
            message: e.status === 500 ? "Server error" : e.message
        };
    }
});


app.use(compress({}));


app.use(body({
    multipart: true,
    formidable: {
        uploadDir: __dirname + '/uploads',
        onFileBegin: function (name, file) {
            console.log(file)
        }
    }
}));
/*
 ключ сессии
 в продакшене должна быть более надежная генерация и способ хранения
 */
if (process.env.NODE_ENV === "production") {
    app.context.secret = process.env.SECRET || (() => {
        throw new Error("No secret key")
    })()
}
else app.context.secret = "dont_look_sempai";


//модуль авторизации и сессии, которые хранятся в монго
app.use(session({store: new MongooseStore()}, app));

/*
    разбиение приложения на админа и юзера
 */
const jwt = require("jsonwebtoken");
const api = require("./common/API/fspoRequester");
const Admin = require("./model/Admin");

app.use(async function (ctx) {
    const token = ctx.request.headers['x-access-token'];
    if (token) {
        let data;
        try {
            data = jwt.verify(token, ctx.secret);
        } catch (err) {
            return ctx.throw(ctx.errorsList.invalid_token)
        }

        const user = await api.getUserData(data.fspo_id, data.fspo_token);
        if (!!(await Admin.findOne({fspo_id: user.fspo_id})))
            user.roles.push("admin");


        ctx.state.user = user;
    }
    await compose(require('./app/user/app').middleware)(ctx);
    if (ctx.state.user && ctx.checkRole("admin"))
        await compose(require('./app/admin/app').middleware)(ctx);


});

app.listen(process.env.PORT || 3000);
console.info(`${process.version} listening on port ${process.env.PORT || 3000} (${app.env})`);


module.exports = app;


