function genErr(status, message){
    const error=new Error();
    error.status=status;
    error.message=message;
    return error;
}
module.exports.list={
    no_such_admin: genErr(401, "No such admin"),
    not_autorized: genErr(401, "Not authorized"),
    wrong_credentials: genErr(403, "Wrong credentials"),
    invalid_token:genErr(403, "Invalid token"),
    invalid_status: genErr(400, "Invalid application status"),
    forbidden: genErr(403, "Forbidden"),
    bad_request: genErr(400, "Bad request"),
    no_file:genErr(404, "Cert file not found"),
    validation_error: genErr(404, "Validation_error"),
    wrong_app_type:genErr(400, "Wrong type, 'forme' or 'my' expected")
};
module.exports.generateErr=genErr;