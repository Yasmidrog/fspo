module.exports = {
    api_hostname: "https://ifspo.ifmo.ru/api",
    app_key: "046736709b9627ad43fb7ac7ad362ca8106b8050",
    statuses: {
        denied: "Отклонен",
        approved: "Подтвержден",
        pending: "Проверка преподавателем",
        moderation: "Проверка администратором"
    }
};